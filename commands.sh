#!/bin/bash

git init
npm init -y
# https://docs.npmjs.com/cli/v7/commands/npm-pkg
# npm help pkg
# npm pkg get version
npm i express dotenv --save
npm i mocha chai --save-dev
npm pkg set scripts.start='node src/main.js'
npm pkg set scripts.test='mocha'
npm pkg set description='CICD project'
# npm pkg set

cat << EOF >> .gitignore
node_modules

.env

.DS_Store
**/.DS_Store
EOF


cat << EOF >> .gitlab-ci.yml
image: node:latest

cache:
  paths:
    - node_modules/

test_converter:
  tags:
    - glrunner
  before_scripts:
    - npm install
  script:
    - npm run test
EOF

cat << EOF >> rest.https
### Works with VSCode Extension "REST Client"

@baseurl=http://localhost:3000

### GET request to root path

GET {{baseurl}}/ HTTP/1.1

### GET /add?a=1&b=2
GET {{baseurl}}/add?a=1&b=2 HTTP/1.1

EOF

mkdir src
touch src/main.js
touch src/router.js

mkdir test
touch test/router.spec.js

# Manage npm version
# https://docs.npmjs.com/cli/v8/commands/npm-version
# note! running version commands may create git commit automatically...
# npm version major
# npm version minor
# npm version patch
# You can reset git to previous version with
# git reset --hard <origin+branch>
