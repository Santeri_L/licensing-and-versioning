const expect = require("chai").expect;
const router = require("../src/router");
const PORT = 3000;
const HOST = "127.0.0.1";
// SCHEME or PROTOCOL
const baseUrl = `http://${HOST}:${PORT}`;

describe("Test routes", () => {
    let server;
    before("Start server", (done) => {
        server = router.listen(
            PORT,
            HOST,
            () => done()
        );
        
    });
    describe("Routes", () => {
        it("Can GET welcome message", async () => {
            const response = await fetch(baseUrl + "/");
            const response_msg = await response.text();
            expect(response_msg).to.equal("welcome!");
        });
        it("Can GET sum of two numbers", async () => {
            const endpoint = baseUrl + "/add";
            const getResponse = (a, b) => new Promise(async (resolve, reject) => {
                const query = `${endpoint}?a=${a}&b=${b}`; // http://example.com/endpoint?a=1&b=2
                const response = await(await fetch(query)).text();
                resolve(response);
            });
            // console.log(await getResponse(1, 2));
            expect(await getResponse(1, 2)).to.equal("3");
            // steps: add feature (multiply), patch bugs, break the API by changing it.
            // console.log(await getResponse("e", "f")); // how to present errors?
            // console.log(await getResponse(Number.MAX_VALUE, 1)); // max + 1?
            // console.log(await getResponse(999_999_999_999_999_999_999, 1)); // representation?
        });
        it("Can GET product of two numbers", async () => {
            const endpoint = baseUrl + "/mul";
            const getResponse = (a, b) => new Promise(async (resolve, reject) => {
                const query = `${endpoint}?a=${a}&b=${b}`; // http://example.com/endpoint?a=1&b=2
                const response = await(await fetch(query)).text();
                resolve(response);
            });

            expect(await getResponse(4, 4)).to.equal("16"); 
        });
        it("Changed behaviour of REST API", async () => {
            const endpoint = baseUrl + "/any/*";
            const getResponse = (a, b, c, d) => new Promise(async (resolve, reject) => {
                const query = `${endpoint}?a=${a}&b=${b}&c=${c}&d=${d}`;
                const response = await(await fetch(query)).text();
                resolve(response);
                // endpoint, which can take any amount of arguments
            });
            expect(await getResponse(4, 5, 6, 7)).to.equal("22"); 
        });
        
        
    });
    after("Close server", () => {
        server.close();
    });
});
