// 3. Cleanup (in listener way)
const exitHandler = (options, exit_code) => {
    console.log(`Service stopped with exit code: ${exit_code}`)
};
['exit', 'SIGINT', 'SIGUSR1', 'SIGUSR2', 'uncaughtException', 'SIGTERM'].forEach((ev) => {
    process.on(ev, exitHandler.bind(null, ev))
});
// 1. Initialize
console.log("Service starting.");
require('dotenv').config();
const router = require('./router');
const getPort = () => {
    let port = undefined;
    try {
        port = parseInt(process.env.PORT, 10);
        if (typeof port !== 'number') {
            throw new Error('Not a number');
        }
    } catch (err) {
        console.error(`Environment variable PORT isn't defined`);
        process.exit(0);
    }
    return port;
};
const PORT = getPort();
const HOST = '0.0.0.0';

// 2. Operate
router.listen(
    PORT,
    HOST,
    () => console.log(`Listening to http://${HOST}:${PORT}`)
);

