const express = require('express');
const router = express();

//router endpoint
router.get("/", (req, res) => res.send("welcome!"));

router.get("/add", (req, res) => {
    try {
        const sum = req.query.a + req.query.b;
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/mul", (req, res) => {
    try {
        const sum = req.query.a * req.query.b;
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

function arraySum(arr) {
    return arr.reduce((total, num) => total + num, 0);
  }
  

router.get("/any/*", (req, res) => {
    try {
        const args = req.params[0].split('/').map(Number);
        const sum = arraySum(args);
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

/** Proper JSON REST API Middlewares */
router.use(express.urlencoded({ extended: true}));
router.use(express.json());

module.exports = router;